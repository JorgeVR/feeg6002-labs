#include <stdio.h>

int main(void) {
	
	double s = 1000.0;
	double debt = s;
	double rate = 0.03;
	double interest;
	double total_interest = 0.0;
	int month;
	
	for(month=1;month<=24;month++) {
		interest = debt * rate;
		total_interest += interest;
		debt += interest;
		printf("month %2d: debt=%7.2f, interest=%.2f, total_interest=%7.2f, frac=%6.2f%%\n",month, debt, interest, total_interest, total_interest*100.0/1000.0);
	}
	
	return 0;
}

