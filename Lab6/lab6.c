/* Laboratory 6, FEEG6002, 2016/2017, Template */

#include <stdio.h>
#include <string.h>

/* Function void rstrip(char s[])
modifies the string s: if at the end of the string s there are one or more spaces,
then remove these from the string.

The name rstrip stands for Right STRIP, trying to indicate that spaces at the 'right'
end of the string should be removed.
*/

void rstrip(char s[]) {
    int i;
	
	i = strlen(s);	/* Function strlen(s) is included inside <string.h> */
	while(s[--i] == ' ');
	s[i + 1] = '\0';	
}

void lstrip(char s[]) {
	int i = 0;
	int lspc;
	
	while(s[i++] == ' ');
	lspc = --i;
	while(s[i] != '\0') {
		s[i - lspc] = s[i];
		i++;
	}
	s[i - lspc] = s[i];	   
}


int main(void) {
  char test1[] = "   Hello World   ";

  printf("Original string reads  : |%s|\n", test1);
  rstrip(test1);
  printf("r-stripped string reads: |%s|\n", test1);
  lstrip(test1);
  printf("l-stripped string reads: |%s|\n", test1);

  return 0;
}

