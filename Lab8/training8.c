#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* mix(char *s1, char *s2);

int main(void) {
	char s1[] = "Hello World";
    char s2[] = "1234567890!";

    printf("s1 = %s\n", s1);
    printf("s2 = %s\n", s2);
    printf("r  = %s\n", mix(s1, s2));
	
	return 0;
}

char* mix(char *s1, char *s2) {
	char *s;
	int sl = strlen(s1);
	int i;
	
	s = (char*) malloc(sizeof(char) * ((sl * 2) + 1));
	if (s != NULL) {
		for(i = 0; i < (2 * sl); i++) {
			s[2 * i] = s1[i];
			s[(2 * i) + 1] = s2[i];
		}
	}
	else {
		printf("Memory allocation failed");
	}
	return s;
}
		
	

