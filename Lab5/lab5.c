#include <stdio.h>
#include <string.h>
#define MAXLINE 1000 /* maximum length of string */

/* function prototype */
void reverse(char source[], char target[]);

int main(void) {
  char original[] = "This is a test: can you print me in reverse character order?";
  char reversed[MAXLINE];

  printf("%s\n", original);
  reverse(original, reversed);
  printf("%s\n", reversed);
  return 0;
}

/* reverse the order of characters in 'source', write to 'target'.
   Assume 'target' is big enough. */
void reverse(char source[], char target[]) {
	int i;
	int len;
	
	len = strlen(source); /* Included in the <string.h> header */
	/* printf("len = %d\n", len); */
	for(i = 0; i < len ; i++) {
		target[len - i -1] = source[i];
		/* printf("source char %c\n",source[i]); */
	}
	target[len] = '\0';	   
}

