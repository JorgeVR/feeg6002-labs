#include <stdio.h>

long string_length(char s[]);

int main(void) {
	char s[]="Hello World";
	
	printf("'%s' has %ld characters", s, string_length(s));	   
	return 0;
}

long string_length(char s[]) {
	long i = 0;
	
	while(s[i++] != '\0');  
	return i - 1;
}

