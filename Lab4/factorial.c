#include <stdio.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>

long maxlong(void);
double upper_bound(long n);
long factorial(long n);

int main(void) {
    long i;

    /* The next line should compile once "maxlong" is defined. */
    printf("maxlong()=%ld\n", maxlong());

    /* The next code block should compile once "upper_bound" is defined. */
    for (i=0; i<22; i++) {
        printf("factorial(%ld)= %ld, upper_bound(%ld)=%g\n",i, factorial(i), i, upper_bound(i));
    }
    
    return 0;
}

long maxlong(void) {
	return LONG_MAX;
}

double upper_bound(long n) {
	if((0 <= n) && (n < 6)) {
		return 719.0;
	}
	else if(n>=6) {
		return pow(n/2.0, n);
	}
	else {
		return -1;
	}
}

long factorial(long n) {
	int i;
	long sum = 1;
	
	if (n<0) {
		return -2;
	}
	else if (maxlong() <= upper_bound(n)) {
		return -1;
	}
	else {
		for(i = 1; i <= n; i++) {
			sum *= i;
			/*if (sum <= 0) {
				ovfl = true;
			}*/
		}
		return sum;
	}
}


